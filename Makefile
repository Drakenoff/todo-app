.PHONY: all clean install run

run_builder:
	@echo "Running build_runner"
	@flutter pub run build_runner build --delete-conflicting-outputs

get_packages:
	@echo "Fetching packages"
	@flutter clean && flutter pub get