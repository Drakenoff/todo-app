# Todo App

This is a Flutter application for managing your daily tasks.

## Getting Started

1. Make sure you are using Flutter 3.19.5 and Dart 3.3.3 or higher versions.
2. Clone this repository.
3. Run `flutter clean && flutter pub get` to install the necessary Dart packages.
4. Run code generation `flutter pub run build_runner build --delete-conflicting-outputs` to generate models.
5. Run `flutter run` to start the application.