sealed class Status {}

class Loading extends Status {}

class Success extends Status {}

class Error extends Status {
  Error(this.message);

  final String message;
}
