enum TodoFormType {
  view,
  create;

  bool get isView => this == TodoFormType.view;
  bool get isCreate => this == TodoFormType.create;
}
