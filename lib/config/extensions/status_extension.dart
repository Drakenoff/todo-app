import 'package:todo_app/config/contract/network_status.dart';

extension ExStatus on Status {
  bool get isLoading => this is Loading;
  bool get isSuccess => this is Success;
  bool get isError => this is Error;
}
