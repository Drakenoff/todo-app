import 'package:flutter/material.dart';

mixin SnackbarMixin {
  void showSnackbar(BuildContext context, String message) =>
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(message)),
      );
}
