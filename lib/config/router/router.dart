import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/core/models/todo_model.dart';
import 'package:todo_app/features/views/home/home_view.dart';
import 'package:todo_app/features/views/todo_form/todo_form_view.dart';

part 'router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'View,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          path: '/',
          page: HomeRoute.page,
          initial: true,
        ),
        AutoRoute(page: TodoFormRoute.page),
      ];
}
