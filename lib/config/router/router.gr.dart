// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    HomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HomeView(),
      );
    },
    TodoFormRoute.name: (routeData) {
      final args = routeData.argsAs<TodoFormRouteArgs>(
          orElse: () => const TodoFormRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: TodoFormView(
          todo: args.todo,
          key: args.key,
        ),
      );
    },
  };
}

/// generated route for
/// [HomeView]
class HomeRoute extends PageRouteInfo<void> {
  const HomeRoute({List<PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [TodoDetailView]
class TodoDetailRoute extends PageRouteInfo<TodoDetailRouteArgs> {
  TodoDetailRoute({
    required TodoModel todo,
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          TodoDetailRoute.name,
          args: TodoDetailRouteArgs(
            todo: todo,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'TodoDetailRoute';

  static const PageInfo<TodoDetailRouteArgs> page =
      PageInfo<TodoDetailRouteArgs>(name);
}

class TodoDetailRouteArgs {
  const TodoDetailRouteArgs({
    required this.todo,
    this.key,
  });

  final TodoModel todo;

  final Key? key;

  @override
  String toString() {
    return 'TodoDetailRouteArgs{todo: $todo, key: $key}';
  }
}

/// generated route for
/// [TodoFormView]
class TodoFormRoute extends PageRouteInfo<TodoFormRouteArgs> {
  TodoFormRoute({
    TodoModel? todo,
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          TodoFormRoute.name,
          args: TodoFormRouteArgs(
            todo: todo,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'TodoFormRoute';

  static const PageInfo<TodoFormRouteArgs> page =
      PageInfo<TodoFormRouteArgs>(name);
}

class TodoFormRouteArgs {
  const TodoFormRouteArgs({
    this.todo,
    this.key,
  });

  final TodoModel? todo;

  final Key? key;

  @override
  String toString() {
    return 'TodoFormRouteArgs{todo: $todo, key: $key}';
  }
}
