import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:todo_app/config/constants/endpoints.dart';
import 'package:todo_app/core/errors/api_exception.dart';
import 'package:todo_app/core/models/todo_model.dart';

final class TodoAPI {
  const TodoAPI(this._client);

  final http.Client _client;

  Future<List<dynamic>> fetchTodos() async {
    final response = await _client.get(Uri.parse('${Endpoints.domain}/todos'));

    if (response.statusCode == 200) {
      return jsonDecode(response.body) as List<dynamic>;
    }

    throw const ServerFailure('Failed to fetch todos');
  }

  Future<dynamic> createTodo(TodoModel todo) async {
    final response = await _client.post(
      Uri.parse('${Endpoints.domain}/todos'),
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      body: jsonEncode(todo.toJson()),
    );

    if (response.statusCode == 201) {
      return jsonDecode(response.body) as dynamic;
    }

    throw const ServerFailure('Failed to create todo');
  }

  Future<dynamic> deleteTodo(int id) async {
    final response = await _client.delete(
      Uri.parse('${Endpoints.domain}/todos/$id'),
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body) as dynamic;
    }

    throw response.statusCode == 404
        ? const NotFoundException('Todo not found')
        : const ServerFailure('Failed to delete todo');
  }
}
