class Failure implements Exception {
  const Failure(this.message);

  final String message;
}

class ServerFailure extends Failure {
  const ServerFailure(super.message);
}

class ConnectionFailure extends Failure {
  const ConnectionFailure(super.message);
}

class NotFoundException extends Failure {
  const NotFoundException(super.message);
}
