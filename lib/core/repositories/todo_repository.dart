import 'dart:async';

import 'package:todo_app/core/models/todo_model.dart';

/// Interface for the todo repository
abstract interface class TodoRepository {
  /// Fetch all todo items
  FutureOr<List<TodoModel>> fetchTodos();

  /// Create a new todo item
  FutureOr<TodoModel> createTodo(TodoModel todo);

  /// Delete a todo item
  Future<void> deleteTodo(int id);
}
