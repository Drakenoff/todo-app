import 'dart:async';
import 'dart:io';

import 'package:todo_app/core/data_sources/todo_api.dart';
import 'package:todo_app/core/errors/api_exception.dart';
import 'package:todo_app/core/models/todo_model.dart';
import 'package:todo_app/core/repositories/todo_repository.dart';

class TodoRepositoryImpl implements TodoRepository {
  const TodoRepositoryImpl(this._todoAPI);

  final TodoAPI _todoAPI;

  @override
  FutureOr<List<TodoModel>> fetchTodos() async {
    try {
      final todos = await _todoAPI.fetchTodos();
      return todos.map((e) => TodoModel.fromJson(e)).toList();
    } on SocketException {
      throw const ConnectionFailure('Failed to connect to the network');
    }
  }

  @override
  FutureOr<TodoModel> createTodo(TodoModel todo) async {
    try {
      final createdTodo = await _todoAPI.createTodo(todo);
      return TodoModel.fromJson(createdTodo);
    } on SocketException {
      throw const ConnectionFailure('Failed to connect to the network');
    }
  }

  @override
  Future<void> deleteTodo(int id) async {
    try {
      await _todoAPI.deleteTodo(id);
    } on SocketException {
      throw const ConnectionFailure('Failed to connect to the network');
    }
  }
}
