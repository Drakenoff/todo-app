import 'package:get_it/get_it.dart';
import 'package:todo_app/core/data_sources/todo_api.dart';
import 'package:todo_app/core/repositories/todo_repository.dart';
import 'package:todo_app/core/repositories/todo_repository_impl.dart';
import 'package:todo_app/features/view_models/home_viewmodel.dart';
import 'package:http/http.dart' as http;

final locator = GetIt.instance;

void setupLocator() {
  /// Repositories
  locator.registerFactory<TodoRepository>(() => TodoRepositoryImpl(locator()));

  /// ViewModels
  locator.registerLazySingleton(() => HomeViewModel(locator()));

  /// Data Sources
  locator.registerFactory(() => TodoAPI(locator()));

  /// External
  locator.registerLazySingleton(() => http.Client());
}
