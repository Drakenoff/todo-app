import 'package:flutter/material.dart';
import 'package:l/l.dart';
import 'package:todo_app/config/contract/network_status.dart';
import 'package:todo_app/config/mixins/snackbar_mixin.dart';
import 'package:todo_app/core/errors/api_exception.dart';
import 'package:todo_app/core/models/todo_model.dart';
import 'package:todo_app/core/repositories/todo_repository.dart';

class HomeViewModel with ChangeNotifier, SnackbarMixin {
  HomeViewModel(this._todoRepo);

  final TodoRepository _todoRepo;

  final _todos = <TodoModel>[];
  List<TodoModel> get todos => _todos;

  Status _status = Success();
  Status get status => _status;

  Future<void> createTodo(
    BuildContext context, {
    required TodoModel todo,
  }) async {
    try {
      final result = await _todoRepo.createTodo(todo);
      _todos.insert(0, result);
      notifyListeners();
    } on Failure catch (e) {
      l.e("Failed to create todo: ${e.message}");
      if (context.mounted) {
        showSnackbar(context, e.message);
      }
    }
  }

  Future<void> fetchTodos() async {
    try {
      updateStatus(Loading());
      final todos = await _todoRepo.fetchTodos();
      _todos.addAll(todos);
      updateStatus(Success());
    } on Failure catch (e) {
      l.e("Failed to fetch todos: ${e.message}");
      updateStatus(Error(e.message));
    }
  }

  void deleteTodo(
    BuildContext context, {
    required int id,
  }) async {
    try {
      final todoIndexAt = _todos.indexWhere((e) => e.id == id);
      if (todoIndexAt == -1) return;

      await _todoRepo.deleteTodo(id);
      _todos.removeAt(todoIndexAt);
      notifyListeners();
    } on Failure catch (e) {
      l.e("Failed to delete todo item: ${e.message}");
      if (context.mounted) {
        showSnackbar(context, "Failed to delete todo item: ${e.message}");
      }
    }
  }

  void updateTodo(TodoModel todo, {int? indexAt}) {
    indexAt ??= _todos.indexWhere((e) => e.id == todo.id);
    if (indexAt == -1 && indexAt < todos.length) return;

    _todos[indexAt] = todo;
    notifyListeners();
  }

  void updateStatus(Status status) {
    _status = status;
    notifyListeners();
  }
}
