import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/config/contract/network_status.dart';
import 'package:todo_app/config/extensions/status_extension.dart';
import 'package:todo_app/config/router/router.dart';
import 'package:todo_app/dependency_injection.dart';
import 'package:todo_app/features/view_models/home_viewmodel.dart';
import 'package:todo_app/features/widgets/loaders/custom_loader.dart';

@RoutePage()
class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    final homeVM = locator<HomeViewModel>();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Todo App"),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: homeVM.fetchTodos,
        child: Consumer<HomeViewModel>(
          builder: (context, value, child) {
            final (status, todos) = (value.status, value.todos);
            if (status.isLoading) return const CustomLoader();
            if (status is Error) return Center(child: Text(status.message));

            return todos.isEmpty
                ? const Center(child: Text('Todo list is empty.'))
                : ListView.builder(
                    itemCount: todos.length,
                    itemBuilder: (context, index) {
                      final todo = todos[index];

                      return ListTile(
                        key: ValueKey(todo.id),
                        onTap: () => context.navigateTo(
                          TodoFormRoute(todo: todo),
                        ),
                        onLongPress: () => homeVM.deleteTodo(
                          context,
                          id: todo.id,
                        ),
                        title: Text(todo.title, maxLines: 1),
                        trailing: Checkbox(
                          value: todo.completed,
                          onChanged: (isDone) => homeVM.updateTodo(
                            todo.copyWith(completed: isDone!),
                            indexAt: index,
                          ),
                        ),
                      );
                    },
                  );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => context.navigateTo(TodoFormRoute()),
        tooltip: 'Create Todo',
        child: const Icon(Icons.add),
      ),
    );
  }
}
