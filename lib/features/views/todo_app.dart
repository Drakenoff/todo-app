import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/config/router/router.dart';
import 'package:todo_app/dependency_injection.dart';
import 'package:todo_app/features/view_models/home_viewmodel.dart';

class TodoApp extends StatelessWidget {
  const TodoApp({super.key});

  @override
  Widget build(BuildContext context) {
    final router = AppRouter();

    return ChangeNotifierProvider(
      create: (_) => locator<HomeViewModel>()..fetchTodos(),
      child: MaterialApp.router(
        title: 'Todo App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        routerConfig: router.config(),
      ),
    );
  }
}
