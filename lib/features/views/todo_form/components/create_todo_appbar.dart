import 'package:flutter/material.dart';

class CreateTodoAppBar extends StatelessWidget {
  const CreateTodoAppBar({this.title, this.onTap, super.key});

  final String? title;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) => AppBar(
        title: Text(title ?? "Create Todo"),
        centerTitle: true,
        actions: [
          if (onTap != null)
            IconButton(
              icon: const Icon(Icons.done_rounded),
              onPressed: onTap,
            ),
        ],
      );
}
