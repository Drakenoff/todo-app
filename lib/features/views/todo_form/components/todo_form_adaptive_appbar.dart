import 'package:flutter/material.dart';
import 'package:todo_app/config/enum/todo_form_type.dart';
import 'package:todo_app/features/views/todo_form/components/create_todo_appbar.dart';
import 'package:todo_app/features/views/todo_form/components/todo_view_appbar.dart';

class AdaptiveTodoFormBar extends StatelessWidget
    implements PreferredSizeWidget {
  const AdaptiveTodoFormBar({
    this.type = TodoFormType.view,
    this.size = 56,
    this.onDeleted,
    this.onSaved,
    super.key,
  });
  final TodoFormType type;
  final double size;
  final VoidCallback? onDeleted;
  final VoidCallback? onSaved;

  @override
  Widget build(BuildContext context) {
    if (type.isView) {
      return TodoViewAppBar(onTap: onDeleted);
    }

    return CreateTodoAppBar(onTap: onSaved);
  }

  @override
  Size get preferredSize => Size.fromHeight(size);
}
