import 'package:flutter/material.dart';

class TodoViewAppBar extends StatelessWidget {
  const TodoViewAppBar({this.title, this.onTap, super.key});

  final String? title;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) => AppBar(
        title: Text(title ?? ''),
        centerTitle: true,
        actions: [
          if (onTap != null)
            IconButton(
              icon: const Icon(Icons.delete_rounded),
              onPressed: onTap,
            ),
        ],
      );
}
