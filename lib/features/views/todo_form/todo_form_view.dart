import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/config/enum/todo_form_type.dart';
import 'package:todo_app/config/mixins/snackbar_mixin.dart';
import 'package:todo_app/core/models/todo_model.dart';
import 'package:todo_app/dependency_injection.dart';
import 'package:todo_app/features/view_models/home_viewmodel.dart';
import 'package:todo_app/features/views/todo_form/components/todo_form_adaptive_appbar.dart';
import 'package:todo_app/features/widgets/app_gap/app_gap.dart';
import 'package:todo_app/features/widgets/text_field/text_form.dart';

@RoutePage()
class TodoFormView extends StatefulWidget {
  const TodoFormView({this.todo, super.key});

  final TodoModel? todo;

  @override
  State<TodoFormView> createState() => _TodoFormViewState();
}

class _TodoFormViewState extends State<TodoFormView> with SnackbarMixin {
  late final TodoModel? todo;
  final _titleCT = TextEditingController();
  final _descriptionCT = TextEditingController();

  @override
  void initState() {
    super.initState();
    todo = widget.todo;
    if (todo == null) return;

    _titleCT.text = todo!.title;
    _descriptionCT.text = todo!.description ?? '';
  }

  @override
  void dispose() {
    super.dispose();
    _titleCT.dispose();
    _descriptionCT.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final homeVM = locator<HomeViewModel>();

    return Scaffold(
      appBar: AdaptiveTodoFormBar(
        type: todo == null ? TodoFormType.create : TodoFormType.view,
        onSaved: () async {
          final title = _titleCT.text;
          final description = _descriptionCT.text;
          if (title.isEmpty) {
            return showSnackbar(context, "Title cannot be empty");
          }

          await homeVM.createTodo(
            context,
            todo: TodoModel(
              id: (homeVM.todos.lastOrNull?.id ?? 0) + 1,
              title: title,
              description: description,
            ),
          );
          if (context.mounted) context.maybePop();
        },
        onDeleted: () {
          homeVM.deleteTodo(context, id: widget.todo!.id);
          context.maybePop();
        },
      ),
      body: AppGap(
        child: AbsorbPointer(
          absorbing: todo != null,
          child: Column(
            children: [
              TextForm(label: "Title", controller: _titleCT, maxLines: 1),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: TextForm(
                  label: "Description",
                  controller: _descriptionCT,
                  maxLines: 3,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
