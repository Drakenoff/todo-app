import 'package:flutter/material.dart';

class TextForm extends StatelessWidget {
  const TextForm({
    required this.label,
    this.controller,
    this.minLines,
    this.maxLines,
    super.key,
  });

  final String label;
  final TextEditingController? controller;
  final int? minLines;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      minLines: minLines,
      maxLines: maxLines,
      decoration: InputDecoration(
        labelText: label,
        alignLabelWithHint: true,
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
      ),
    );
  }
}
