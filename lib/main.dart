import 'dart:async';

import 'package:flutter/material.dart';
import 'package:l/l.dart';
import 'package:todo_app/dependency_injection.dart';
import 'package:todo_app/features/views/todo_app.dart';

Future<void> main() async => runZonedGuarded(
      () async {
        WidgetsFlutterBinding.ensureInitialized();
        setupLocator();
        runApp(const TodoApp());
      },
      (error, stackTrace) => l.e('Top-level\nerror: $error', stackTrace),
    );
